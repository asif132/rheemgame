<?php
App::uses('AppModel', 'Model');
/**
 * Page Model
 *
 */
class Page extends AppModel {


	public $actsAs = array(
        'Upload.Upload' => array(
            'resim' => array(
				'path' => '{ROOT}webroot{DS}images{DS}',
				'thumbnailSizes' => array(
					'thumb' => '500x500'
				),
				'thumbnailMethod' => 'php'
			)
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'baslik' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'resim' => array(
		),
		'slug' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
