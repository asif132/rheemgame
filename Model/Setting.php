<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class Setting extends AppModel {

	public $site_settings = array();

	public $actsAs = array(
		'Containable'
    );
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'key' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'value' => array(
		),
	);
	
	//retrieve configuration data from the DB
    function get_settings(){
		$this->site_settings = $this->find('list', array('fields'=>array('key','value')));
    }
}
