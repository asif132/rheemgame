<div id="wrap">
      <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <?php echo $this->Html->link($site_settings['site_name'], '/admin', array('class' => 'navbar-brand'));?>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">

                  <li<?php if($this->params['controller'] == 'pages'): ?> class="active"<?php endif;?>><?php echo $this->Html->link(__('Pages'), array('controller' => 'pages', 'action' => 'index')); ?></li>
                  <li<?php if($this->params['controller'] == 'levels'): ?> class="active"<?php endif;?>><?php echo $this->Html->link(__('Levels'), array('controller' => 'levels', 'action' => 'index')); ?></li>
                  <li<?php if($this->params['controller'] == 'questions'): ?> class="active"<?php endif;?>><?php echo $this->Html->link(__('Questions'), array('controller' => 'questions', 'action' => 'index')); ?></li>
                  <li<?php if($this->params['controller'] == 'users'): ?> class="active"<?php endif;?>><?php echo $this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index')); ?></li>

                    <li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
                </ul>
            </div><!--/.nav-collapse -->
      </div>