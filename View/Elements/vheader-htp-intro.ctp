    <header class="how-to-play-header">
        <div class="row">
            <div class="col-3">
                <?php echo html_entity_decode($this->Html->link($this->Html->image("btn-back.png"), array("controller" => "pages", "action" => "vgamehome"), array("class" => "btn-back")));?>
            </div>
            <div class="col-9">
                <?php echo $this->Html->image("intro.png", array("class" => "Video Game Introduction"))?>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php echo $this->Html->image("seperator-htp.png", array("class" => "seperator-htp"))?>
    </header>