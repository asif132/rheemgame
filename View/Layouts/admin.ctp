<!DOCTYPE html>
<!--
     :syyyyyyyyyyyo-                    
   :shhhhhhhhhhhs-                      
 :shhhhhhhhhhhs-                        
yhhhhhhhhhhhs-                          
mmdhhhhhhhs-                            
mmmmdhhhs-                              
mmmmmmh-            ``````````````      
mmmmmmmd/            .::::::::::::-`    
mmmmmmmmmd/            .::::::::::::-`  
mmmmmmmmmmmd+            .::::::::::::-`
-ymmmmmmmmmmmd+`           .::::::::::/o
  -ymmmmmmmmmmmd+`           .::::::/oss
    -ymmmmmmmmmmmd+`           .::/ossss
      .::::::::::::-            .ossssss
                              .+ssssssss
                            .+ssssssssss
                          .+ssssssssssso
                        .+ssssssssssso- 
                      .+ssssssssssso-   
                    .+ssssssssssso-     
=========================================
     Salinus Bilgi Teknolojileri
=========================================
-->
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<title>
		<?php echo $site_settings['site_name'].' - Admin Panel'; ?>
	</title>
	<?php
		echo $this->Html->meta(
		'favicon.ico',
		$this->Html->url('/img/admin_icon.png'),
		array('type' => 'icon')
		);
		
        echo $this->Html->script('jquery-2.1.0.min.js');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->css('bootstrap.admin.min');
		echo $this->Html->css('admin_style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

<?php echo $this->element('admin/header'); ?>
<div class="container">

	<?php 
	$message = $this->Session->flash();
	if($message != NULL): ?>
    <?php echo $message; ?>
	<?php endif; ?>
    
    <?php echo $this->fetch('content'); ?>
  

</div>
<?php echo $this->element('sql_dump'); ?>
<?php echo $this->element('admin/footer'); ?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyA3GCmcLnzE1qjaQzwQYpBOdIJ3OOnFLF0&sensor=false&amp;language=en"></script>
<?php
	echo $this->Html->script('iller.js');
	echo $this->Html->script('gmap3.min.js');
	echo $this->Html->script('harita_admin.js');
	echo $this->fetch('script');
?>

<?php 
echo $this->Html->script('/ckeditor/ckeditor', false); 
echo $this->fetch('script');
?>
</body>
</html>
