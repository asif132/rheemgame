<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo __('Login'); ?>
	</title>
	<?php
		echo $this->Html->meta(
		'favicon.ico',
			$this->Html->url('/img/admin_icon.png'),
			array('type' => 'icon')
		);
		
		echo $this->Html->script('jquery-1.12.0.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->css('bootstrap.admin.min');
		echo $this->Html->css('admin_style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="login-form-holder">
	<?php echo $this->Html->image('logo-admin.png', array('class' => 'img-responsive', 'style' => 'max-width:150px;margin:auto')); ?>
    <?php echo $this->fetch('content'); ?>
	<?php 
	$error = $this->Session->flash('error');
	if($error != NULL): ?>
	<div class="alert alert-danger">  
		<a class="close" data-dismiss="alert">&times;</a>  
		<strong><?php echo $error; ?></strong>
        <?php if( $this->Form->error('User.username') != '') echo '<p>'.$this->Form->error('User.username').'</p>';?>
        <?php if( $this->Form->error('User.email') != '') echo '<p>'.$this->Form->error('User.email').'</p>';?>
	</div>
	<?php endif; ?>
	<?php 
	$warning = $this->Session->flash('warning');
	if($warning != NULL): ?>
	<div class="alert alert-warning">  
		<a class="close" data-dismiss="alert">&times;</a>  
		<strong><?php echo $warning; ?></strong>
	</div>
	<?php endif; ?>
	<?php 
	$success = $this->Session->flash('success');
	if($success != NULL): ?>
	<div class="alert alert-success">  
		<a class="close" data-dismiss="alert">&times;</a>  
		<strong><?php echo $success; ?></strong>
	</div>
	<?php endif; ?>
	<?php 
	$message = $this->Session->flash();
	if($message != NULL): ?>
    <div class="alert alert-info">  
        <a class="close" data-dismiss="alert">&times;</a>  
        <strong><?php echo $message; ?></strong> 
    </div>
	<?php endif; ?>
</div>
</body>
</html>