<!DOCTYPE html>
<html <?php if($html_class != "") echo 'class="'.$html_class.'"'; ?>>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<title>
		<?php echo $site_settings['site_name'].' - Admin Panel'; ?>
	</title>
	<?php
		echo $this->Html->meta(
		'favicon.ico',
		$this->Html->url('/img/admin_icon.png'),
		array('type' => 'icon')
		);
		
		echo $this->Html->script('jquery-3.3.1.min');
		echo $this->Html->script('/js/jquery-ui-1.12.1/jquery-ui.min.js');
		echo $this->Html->script('jquery.ui.touch-punch.min.js');
		echo $this->Html->script('vgame.js');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <!-- Preloader -->
    <style type="text/css">
      .loader-overlay {
          width: 100%;
          height: 100%;
          background: url('./img/preloader2.gif') center no-repeat rgba(0,0,0,0.8);
          background-size: 230px;
          z-index: 99999;
          position: fixed;
          top:0;
          left: 0;
          right: 0;
          bottom: 0;
      }
    </style>
    <style>.loader-overlay { display: none; } </style>
    <!-- Preloader End -->
</head>
<body>
<div class="loader-overlay"></div>
<?php echo $this->fetch('content'); ?>
<?php echo $this->element('sql_dump'); ?>
<?php 
echo $this->fetch('script');
?>
</body>
</html>