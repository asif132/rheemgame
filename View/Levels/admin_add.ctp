<div class="levels form">
<?php echo $this->Form->create('Level', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Level'); ?></legend>
	<?php
		echo $this->Form->input('level_number', array('class' => 'form-control'));
		echo $this->Form->input('level_title', array('class' => 'form-control'));
		echo $this->Form->input('splash_title', array('class' => 'form-control'));
		echo $this->Form->input('splash_text', array('class' => 'form-control'));
		echo $this->Form->input('splash_image', array('type' => 'file'));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-warning'));
echo $this->Form->end(); ?>
</div>