<div class="levels form">
<?php echo $this->Form->create('Level', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Level'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('level_number');
		echo $this->Form->input('level_title');
		echo $this->Form->input('splash_title');
		echo $this->Form->input('splash_text');
		echo $this->Form->input('splash_image', array('type' => 'file'));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-warning'));
echo $this->Form->end(); ?>
</div>