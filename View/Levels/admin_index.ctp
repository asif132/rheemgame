<div class="levels index">
	<h2><?php echo __('Levels'); ?><?php echo $this->Html->link(__('Create New Level'), array('action' => 'add'), array('class' => 'pull-right btn btn-info')); ?></h2>
	<table class="table table-striped table-hover table-condensed table-responsive">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_number'); ?></th>
			<th><?php echo $this->Paginator->sort('level_title'); ?></th>
			<th><?php echo $this->Paginator->sort('splash_title'); ?></th>
			<th><?php echo $this->Paginator->sort('splash_text'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
    <tbody class="table-bordered">
	<?php foreach ($levels as $level): ?>
	<tr>
		<td><?php echo h($level['Level']['id']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['level_number']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['level_title']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['splash_title']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['splash_text']); ?>&nbsp;</td>
		<td><?php echo h($level['Level']['splash_image']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $level['Level']['id']),array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $level['Level']['id']), array('class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # %s?', $level['Level']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous page'), array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default prev disabled'));
		echo $this->Paginator->numbers(array('separator' => '' ,'class' => 'btn btn-default'));
		echo $this->Paginator->next(__('next page') . ' >', array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default next disabled'));
	?>
	</div>
</div>