<div class="levels view">
<h2><?php echo __('Level'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($level['Level']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level Number'); ?></dt>
		<dd>
			<?php echo h($level['Level']['level_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level Title'); ?></dt>
		<dd>
			<?php echo h($level['Level']['level_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Splash Title'); ?></dt>
		<dd>
			<?php echo h($level['Level']['splash_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Splash Text'); ?></dt>
		<dd>
			<?php echo h($level['Level']['splash_text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Splash Image'); ?></dt>
		<dd>
			<?php echo h($level['Level']['splash_image']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Level'), array('action' => 'edit', $level['Level']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Level'), array('action' => 'delete', $level['Level']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $level['Level']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Levels'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Level'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($level['Question'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th><?php echo __('Question Text'); ?></th>
		<th><?php echo __('Answers'); ?></th>
		<th><?php echo __('Badge Title'); ?></th>
		<th><?php echo __('Badge Image'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($level['Question'] as $question): ?>
		<tr>
			<td><?php echo $question['id']; ?></td>
			<td><?php echo $question['level_id']; ?></td>
			<td><?php echo $question['question_text']; ?></td>
			<td><?php echo $question['answers']; ?></td>
			<td><?php echo $question['badge_title']; ?></td>
			<td><?php echo $question['badge_image']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $question['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
