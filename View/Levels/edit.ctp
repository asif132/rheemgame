<div class="levels form">
<?php echo $this->Form->create('Level'); ?>
	<fieldset>
		<legend><?php echo __('Edit Level'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('level_number');
		echo $this->Form->input('level_title');
		echo $this->Form->input('splash_title');
		echo $this->Form->input('splash_text');
		echo $this->Form->input('splash_image');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Level.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Level.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Levels'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
