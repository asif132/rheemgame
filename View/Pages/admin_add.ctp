<div class="pages form">
<?php echo $this->Form->create('Page', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Create New Page'); ?></legend>
	<?php
		echo $this->Form->input('baslik', array('label' => 'Title', 'class' => 'form-control'));
		echo $this->Form->input('dosya', array('label' => 'Image', 'type' => 'file'));
		echo $this->Form->input('icerik', array('label' => 'Content', 'class' => 'form-control ckeditor'));
		echo $this->Form->input('meta_description', array('class' => 'form-control'));
		echo $this->Form->input('meta_keywords', array('class' => 'form-control'));
	?>
	</fieldset>
<?php 
echo $this->Form->submit(__('Save'), array('class' => 'btn btn-warning'));
echo $this->Form->end(); ?>
</div>