<div class="pages index">
	<h2><?php echo __('Pages'); ?><?php echo $this->Html->link(__('Create New Page'), array('action' => 'add', 'haber'), array('class' => 'pull-right btn btn-info')); ?></h2>
	<table class="table table-striped table-hover table-condensed table-responsive">
    <thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('slug'); ?></th>
			<th><?php echo $this->Paginator->sort('meta_description'); ?></th>
			<th><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
    <tbody class="table-bordered">
	<?php foreach ($pages as $page): ?>
	<tr>
		<td><?php echo h($page['Page']['id']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['baslik']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['resim']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['slug']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['meta_description']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['meta_keywords']); ?>&nbsp;</td>
		<td><?php echo h($page['Page']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $page['Page']['id']),array('class' => 'btn btn-default')); ?>
			<?php echo $this->Html->link(__('Display'), array('admin' => false, 'controller' => 'pages', 'action' => $page['Page']['tip'], $page['Page']['slug']),array('class' => 'btn btn-default', 'target' => '_blank')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $page['Page']['id']), array('class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete this page?'))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	/*
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous page'), array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default prev disabled'));
		echo $this->Paginator->numbers(array('separator' => '' ,'class' => 'btn btn-default'));
		echo $this->Paginator->next(__('next page') . ' >', array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default next disabled'));
	?>
	</div>
</div>