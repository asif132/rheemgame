<div class="pages view">
<h2><?php echo __('Page'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($page['Page']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Baslik'); ?></dt>
		<dd>
			<?php echo h($page['Page']['baslik']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resim'); ?></dt>
		<dd>
			<?php echo h($page['Page']['resim']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('slug'); ?></dt>
		<dd>
			<?php echo h($page['Page']['slug']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Icerik'); ?></dt>
		<dd>
			<?php echo h($page['Page']['icerik']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($page['Page']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sifre'); ?></dt>
		<dd>
			<?php echo h($page['Page']['sifre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dil'); ?></dt>
		<dd>
			<?php echo h($page['Page']['dil']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Description'); ?></dt>
		<dd>
			<?php echo h($page['Page']['meta_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meta Keywords'); ?></dt>
		<dd>
			<?php echo h($page['Page']['meta_keywords']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($page['Page']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Page'), array('action' => 'edit', $page['Page']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Page'), array('action' => 'delete', $page['Page']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $page['Page']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Pages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Page'), array('action' => 'add')); ?> </li>
	</ul>
</div>
