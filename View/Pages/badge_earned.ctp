<div class="content-wrapper">
    <header class="badge-earned-header">
        <h1>WAY TO GO!</h1>
        <h2>YOU'VE EARNED A NEW BADGE</h2>
    </header>
    <h3 class="badge-title level-<?php echo $current_level; ?>"><?php echo $levels[$current_level-1]['Question'][$current_question-1]['badge_title']?></h3>
    <div class="earned-badge-image">
        <?php echo $this->Html->image('/question_images/'.$levels[$current_level-1]['Question'][$current_question-1]['id']."/".$levels[$current_level-1]['Question'][$current_question-1]['badge_image'])?>
    </div>
    <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'game')); ?>" class="skip-to-next"></a>
    <div class="next-button-wrapper">
        <?php echo $this->Html->image("btn-next.png", array("url" => array("controller" => "pages", "action" => "game")))?>
    </div>
</div>