<div class="content-wrapper">
    <header class="badges-completed-header">
        <div class="tilt"></div>
        <h1>BADGES EARNED</h1>
    </header>

    <div class="badges-wrapper">
    <?php $i = 1;
    foreach ($levels as $key => $level): ?>
        <?php if($i %2 == 1): ?>
        <div class="col-6">
       		<?php echo $this->Html->image('/img/badges/group-'.$i.'.png')?>
        </div>
	    <?php endif;?>
        <div class="col-6">
    		<?php $questionI = 1; foreach ($level['Question'] as $question):  ?>
            <?php if( $cookie_levels_data[$i][$questionI] == "1" ):?>
            <div class="col-6">
       			<?php echo $this->Html->image('/question_images/'.$question['id']."/".$question['badge_image'])?>
       			<p class="collected-badges-title"><?php echo $question['badge_title']?></p>
            </div>
            <?php endif; ?>
            <?php if($questionI %2 == 0) echo '<div class="clearfix"></div>'; $questionI++; ?>
	        <?php endforeach;?>
        </div>
        <?php if($i %2 == 0): ?>
        <div class="col-6">
       		<?php echo $this->Html->image('/img/badges/group-'.$i.'.png')?>
        </div>
	    <?php endif;?>
        <div class="clearfix"></div>
	<?php $i++; endforeach; ?>
    </div>
    <div class="clearfix"></div>

    <div class="next-level-button-wrapper">
	<?php echo $this->Form->create('Question', array("id" => "question-form")); ?>
		<?php
            echo $this->Form->input('current_level', array('type' => 'hidden', 'value' => $current_level));
            echo $this->Form->input('badges_collected_seen', array('type' => 'hidden', 'value' => 1));
			if($current_level!=4){
		?>
    	<input type="image" src="<?php echo $this->Html->url("/img/btn-next-level.png")?>"/>
	<?php } echo $this->Form->end(); if($current_level==4){?>
    <?php echo html_entity_decode($this->Html->link($this->Html->image("btn-next-level.png"), array("controller" => "pages", "action" => "vgamehome"), array("class" => "main-screen-buttons")));?>
    <?php } ?>
	</div>

</div>