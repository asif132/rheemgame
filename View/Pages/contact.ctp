<!-- Titlebar
================================================== -->
<section class="titlebar margin-bottom-0">
<div class="container">
    <div class="sixteen columns">
        <h2>İletişim</h2>

        <nav id="breadcrumbs">
            <ul>
                <li><?php echo $this->Html->link("İletişim", array('controller' => 'pages', 'action' => 'home') )?></li>
                <li>İletişim</li>
            </ul>
        </nav>
    </div>
</div>
</section>

<!-- Start Map-->
<section>
    <div class="map-box clearfix">
        <div id="map_container">
            <div id="map-canvas-iletisim" style="height:400px;width: 100%;padding: 0"></div>
        </div>
    </div>
</section>
<!-- End Map-->

<!-- Container -->
<div class="container">
    <div class="four columns">

        <!-- Information -->
        <div class="widget margin-top-10">
            <div class="accordion">
                <!-- Section 2 -->
                <h3><span class="ui-accordion-header-icon ui-icon ui-accordion-icon"></span><i class="fa fa-thumb-tack"></i> Adres</h3>
                <div>
                    <ul class="contact-informations margin-top-0">
                        <li><h4>Merkez:</h4></li>
                        <li><span class="address">Mahmutbey Mah. İstoç 19. Yol Sk. 17. Ada No:157-159 Bağcılar / İstanbul</span></li>
                        <li><i class="fa fa-phone"></i> <p>(0212) 659 3629</p></li>
                        <li><i class="fa fa-envelope"></i> <p><a href="mailto:info@setakirtasiye.com.tr">info@setakirtasiye.com.tr</a></p></li>

                        <li><h4>Şube:</h4></li>
                        <li><span class="address">Tahtakale Cad. No: 27 Eminönü - Fatih / İstanbul</span></li>
                        <li><i class="fa fa-phone"></i> <p>(0212) 527 7813</p></li>
                        <li><i class="fa fa-envelope"></i> <p><a href="mailto:info@setakirtasiye.com.tr">info@setakirtasiye.com.tr</a></p></li>
                    </ul>
                </div>

            </div>
        </div>

    </div>

    <!-- Contact Form -->
    <div class="twelve columns">
        <div class="extra-padding left">
            <h3 class="headline">İletişim</h3><span class="line margin-bottom-25"></span><div class="clearfix"></div>

            <div>
                <?php 
                $message = $this->Session->flash();
                if($message != NULL): ?>
                <?php echo $message; ?>
                <?php endif; ?>
            </div>

            <!-- Contact Form -->
            <section id="contact">

                <!-- Success Message -->
                <mark id="message"></mark>

                <!-- Form -->
                <?php echo $this->Form->create('Contact'); ?>

                    <fieldset>

                        <div>
                            <?php echo $this->Form->input('isim', array('class' => 'form-control', 'div' => false, 'label' => __('İsim'), 'placeholder' => 'İsim')); ?>
                        </div>

                        <div>
                            <?php echo $this->Form->input('email', array('class' => 'form-control', 'div' => false, 'label' => __('Email'), 'placeholder' => 'Email')); ?>
                        </div>

                        <div>
                            <?php echo $this->Form->input('telefon', array('class' => 'form-control', 'div' => false, 'label' => __('Telefon'), 'placeholder' => 'Telefon')); ?>
                        </div>

                        <div>
                            <?php echo $this->Form->input('mesaj', array('class' => 'form-control', 'div' => false, 'label' => __('Mesaj'), 'placeholder' => 'Mesaj')); ?>
                        </div>

                    </fieldset>
                    <div id="result"></div>
                    <?php echo $this->Form->submit(__('Mesajı Gönder'), array('class' => 'submit', 'id' => 'submit'));?>
                    <div class="clearfix"></div>
                <?php echo $this->Form->end(); ?>

            </section>
            <!-- Contact Form / End -->
        </div>
    </div>
</div>
<!-- Container / End -->