<div class="content-wrapper">
    <header class="fuel-up-header">
        <h1>FUEL UP!</h1>
        <h2 class="level-<?php echo $current_level;?>">LEVEL <?php echo $current_level; ?></h2>
        <div class="clearfix"></div>
        <?php echo $this->Html->image("seperator-htp.png", array("class" => "seperator-htp"))?>
    </header>

    <div class="fuelup-content-holder">
        <?php echo $this->Html->image('/level_images/'.$levels[$current_level-1]['Level']['id']."/".$levels[$current_level-1]['Level']['splash_image'])?>
        <p class="title"><?php echo $levels[$current_level-1]['Level']['splash_title']?></p>
        <p class="content"><?php echo $levels[$current_level-1]['Level']['splash_text']?></p>
    </div>
    <div class="next-button-wrapper">

		<?php echo $this->Form->create('Question', array("id" => "question-form")); ?>
			<?php
                echo $this->Form->input('current_level', array('type' => 'hidden', 'value' => $current_level));
                echo $this->Form->input('fuelup_seen', array('type' => 'hidden', 'value' => 1));
			?>
        	<input type="image" src="<?php echo $this->Html->url("/img/btn-next.png")?>"/>
		<?php echo $this->Form->end(); ?>
    </div>
</div>