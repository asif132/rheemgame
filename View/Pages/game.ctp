 <div class="content-wrapper uuu">
    <header class="game-header">
        <div class="row">
            <div class="col-3">
        		<?php echo html_entity_decode($this->Html->link($this->Html->image("btn-back.png"), array("controller" => "pages", "action" => "home"), array("class" => "btn-back")));?>
            </div>
            <div class="col-6 level-<?php echo $current_level; ?>">
                <h1 class="level-indicator"><?php echo $levels[$current_level-1]['Level']['level_title']; ?></h1>
                <h1 class="level-indicator">Level <?php echo $current_level; ?></h1>
            </div>
			<div class="fixed-content-holder">
				<div class="stop-watch-holder">
					<?php echo $this->Html->image("icon-stop-watch.png")?>
					<p>:90</p>
				</div>
				<div class="question-hints-holder">
					<a class="undo-button" href="#"><?php echo $this->Html->image("btn-undo.png")?></a>
					<a class="hint-button" href="#"><?php echo $this->Html->image("btn-hint.png")?></a>
					<p class="hint-button-indicator hint-1">1<span>X</span></p>
					<p class="hint-button-indicator hint-2">2<span>X</span></p>
					<p class="hint-button-indicator hint-3">3<span>X</span></p>
					<p class="hint-button-indicator hint-4">4<span>X</span></p>
					<p class="hint-button-indicator hint-5">5<span>X</span></p>
				</div>
			</div>
        </div>
        <div class="clearfix"></div>
        <?php echo $this->Html->image("seperator-htp.png", array("class" => "seperator-game"))?>
    </header>


    <div class="question-area">
        <div class="question-text-holder">
            <p>
        	<?php
        	$answerChars = array();
        	$words = array();
			$word = "";

        	$chars = str_split($question['question_text']);
        	$answerkey = false;

			foreach($chars as $char){
				if($char != "[" && $char != "]" && !$answerkey ){
					echo $char;
				}elseif($char == "[" ){
					$answerkey = true;
                    echo "<b>";
				}elseif($char == "]" ){
					$answerkey = false;
					$words[] = $word;
					$word = "";
                    echo "</b>";
				}else{
					echo "<span class='letter'>_</span>";
					$answerChars[] = $char;
					$word .= $char;
				}
			}
        	?>
        	</p>
        </div>
    </div>
    <div class="answer-area">
    	<?php
    	$dropIndex = 0;
    	foreach($words as $word): ?>
        <div class="word-holder">
    		<?php for($i = 0; $i < strlen($word); $i++ ):?>
            <div class="letter-drop-box" data-id="<?php echo $dropIndex; ?>"></div>
    		<?php 
			$dropIndex++; 
			endfor;?>
    	</div>
    	<?php endforeach;?>
    </div>
    <div class="letters-area">
    	<?php
    		shuffle($answerChars);
    		$letterIndex = 0;
    		foreach($answerChars as $letter):
    	?>
        <div class="letter-item" data-letter-id="<?php echo $letterIndex?>" data-dropped-index="-1"><?php echo $letter;?></div>
    	<?php
			$letterIndex++; 
			endforeach;
		?>
    </div>

<?php echo $this->Form->create('Question', array("id" => "question-form")); ?>
	<?php
		echo $this->Form->input('submission', array('id' => 'levelcompleted', 'type' => 'hidden', 'value' => '0'));
		echo $this->Form->input('current_level', array('type' => 'hidden', 'value' => $current_level));
		echo $this->Form->input('current_question', array('type' => 'hidden', 'value' => $current_question));
	?>
<?php 
echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
	finalString = "<?php echo implode("", $words);?>"
</script>>