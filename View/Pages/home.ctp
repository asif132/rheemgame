    <div class="content-wrapper">
        <?php echo $this->Html->image("main-game-title.png", array("class" => "main-image-logo"))?>

        <?php echo html_entity_decode($this->Html->link($this->Html->image("btn-how-to-play.png"), array("controller" => "pages", "action" => "how_to_play"), array("class" => "main-screen-buttons")));?>
        <?php echo html_entity_decode($this->Html->link($this->Html->image("btn-lets-play.png"), array("controller" => "pages", "action" => "game"), array("class" => "main-screen-buttons")));?>
        <?php echo $this->Html->image("main_sub_logo.png", array("class" => "main-image-logo-sub"))?>
        
    </div>