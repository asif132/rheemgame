<div class="content-wrapper">
    <?php echo $this->element("header-htp")?>
    <div class="htp-content-holder">
        <?php echo $this->Html->image("htp_content.png")?>
    </div>
    <div class="next-button-wrapper">
        <?php echo $this->Html->image("btn-next.png", array("url" => array("controller" => "pages", "action" => "how_to_play2")))?>
    </div>
</div>
<div class="clearfix"></div>