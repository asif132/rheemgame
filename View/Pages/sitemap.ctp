<?php foreach ($pages as $page):?>
    <url>
        <loc><?php echo $this->Html->url(array('controller' => 'pages', 'action' => $page['Page']['tip'], $page['Page']['slug']), true); ?></loc>
        <changefreq>weekly</changefreq>
		<xhtml:link 
			rel="alternate"
			hreflang="tr"
			href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => $page['Page']['tip'], $page['Page']['slug']), true); ?>"
			/>
    </url>
<?php endforeach;?>
<?php foreach ($products as $product):?>
    <url>
        <loc><?php echo $this->Html->url(array('controller' => 'products', 'action' => 'view', $product['Product']['slug']), true); ?></loc>
        <changefreq>weekly</changefreq>
		<xhtml:link 
			rel="alternate"
			hreflang="tr"
			href="<?php echo $this->Html->url(array('controller' => 'products', 'action' => 'view', $product['Product']['slug']), true); ?>"
			/>
    </url>
<?php endforeach;?>