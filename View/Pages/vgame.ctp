 <div class="content-wrapper">
    <div class="question-area-video">
         
         
         <?php if($current_question > 0 and $current_question < 4){ ?>
         
         <video width="100%" height="340" controls class="videoplayer" poster="<?php echo $this->webroot?>img/2Scenario1.png">
              <source src="<?php echo $this->webroot?>videos/YYC_Audio2.mp4" type="video/mp4">
               Your browser does not support the video tag.
         </video>
         
         <?php } ?>
         
         <?php if($current_question > 3 and $current_question < 6){ ?>
         
         <video width="100%" height="340" controls class="videoplayer" poster="<?php echo $this->webroot?>img/3Scenario2.png">
              <source src="<?php echo $this->webroot?>videos/YYC_Audio3.mp4" type="video/mp4">
               Your browser does not support the video tag.
         </video>
         
         <?php } ?>
         
         <?php if($current_question > 5 and $current_question < 8){ ?>
         
         <video width="100%" height="340" controls class="videoplayer" poster="<?php echo $this->webroot?>img/4Scenario3.png">
              <source src="<?php echo $this->webroot?>videos/YYC_Audio4.mp4" type="video/mp4">
               Your browser does not support the video tag.
         </video>
         
         <?php } ?>
         <?php if($current_question > 7 and $current_question < 10){ ?>
         
         <video width="100%" height="340" controls class="videoplayer" poster="<?php echo $this->webroot?>img/5Scenario4.png">
              <source src="<?php echo $this->webroot?>videos/YYC_Audio5.mp4" type="video/mp4">
               Your browser does not support the video tag.
         </video>
         
         <?php } ?>
		
        <div class="video_question">
           <div class="row">
           
           
           <?php if($current_question == 1){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("thumb_mark.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                 When thinking about the competency <strong>Promoting Entrepreneurship</strong>, what behavior did Max demonstrate best?
                </div>
                
            <?php } ?>
            
            <?php if($current_question == 2){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("EA.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                   When thinking about the competency <strong>Exhibiting Agility</strong>, what behavior did Max demonstrate best?
                </div>
                
            <?php } ?>
            
            <?php if($current_question == 3){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("DC.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                   When thinking about the competency <strong>Driving Collaboration</strong>, what behaviors does Max need to work on most? (Select 2)
                </div>
                
            <?php } ?>
            
            
            <?php if($current_question == 4){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("EA.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                    When thinking about the competency  <strong>Exhibiting Agility</strong>, what behavior did Phillip exhibit best?
                </div>
                
            <?php } ?>
            
            
            <?php if($current_question == 5){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("thumb_mark.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                    When thinking about the competency <strong>Promoting Entrepreneurship</strong>, what behavior does Philip need to work on most?
                </div>
                
            <?php } ?>
            
             <?php if($current_question == 6){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("AI.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                    When thinking about the competency <strong>Accelerating Impact</strong> in this scenario, what behavior did Brie exhibit best?
                </div>
                
            <?php } ?>
            
            
             <?php if($current_question == 7){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("AI.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                     When thinking about the competency <strong>Accelerating Impact</strong> in this scenario, what behavior does Brie need to work on most?
                </div>
                
            <?php } ?>
            
            
      
            
            <?php if($current_question == 8){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("EA.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                      When thinking about the competency <strong>Exhibiting Agility</strong>, what behaviors did Fred exhibit best? (Select 2)
                </div>
                
            <?php } ?>
            
            <?php if($current_question == 9){ ?>
           
                <div class="col-3 video_question_text">
                <?php echo $this->Html->image("DC.png", array("class" => "video-question-icon"))?>
                </div>
                
                <div class="col-9 video_question_text_right">
                       When thinking about the competency <strong>Driving Collaboration</strong>, what behavior does Fred need to work on the most?
                </div>
                
            <?php } ?>
            
             <?php if($current_question == 10){ ?>
             	<div class="col-12">
             	<p class="thanks_msg">Thanks For Playing</p>
                
                <p class="rheem_footer_logo"><?php echo $this->Html->image("footer_logo.png", array("class" => "footer_logo"))?></p>
             	</div>
             <?php } ?>
                
            </div>
        </div> <!-- .video_question -->
        
    </div>
   <?php echo $this->Form->create('Question', array("id" => "question-form-video")); ?>
   
   
   <?php if($current_question == 1){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Demonstrating Ownership and Accountability</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Embodying Strong Self Awareness</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Developing Yourself</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Exhibiting Initiative and Risk Taking</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   
   <?php if($current_question == 2){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Innovating</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Problem Solving & Decision Making</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Engaging Change</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Practicing Followership</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   <?php if($current_question == 3){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel dc_color"><input type="checkbox" name="answerquestion1[]" value="1" id="muhRadio1" class="vquestioninput"/> <span>Practicing Teamwork</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel dc_color"><input type="checkbox" name="answerquestion1[]" value="2" id="muhRadio2" class="vquestioninput"/> <span>Building Partnerships</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel dc_color"><input type="checkbox" name="answerquestion1[]" value="3" id="muhRadio3" class="vquestioninput"/> <span>Communicating Effectively</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel dc_color"><input type="checkbox" name="answerquestion1[]" value="4" id="muhRadio4" class="vquestioninput"/> <span>Establishing & Maintaining Trust</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   
   <?php if($current_question == 4){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Innovating</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Problem Solving and Decision Making</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Engaging Change</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel ea_color"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Practicing Followership</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   <?php if($current_question == 5){ ?>
   
    <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Demonstrating Ownership and Accountability</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Embodying Strong Self Awareness</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Developing Yourself</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Exhibiting Initiative and Risk Taking</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   
   
    <?php if($current_question == 6 or $current_question == 7){ ?>
   
    <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel ai_color"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Achieving Results</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel ai_color"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Focusing on Service</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel ai_color"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Influencing</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel ai_color"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Handling Multiple Priorities</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   <?php if($current_question == 8){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel ea_color"><input type="checkbox" name="answerquestion1[]" value="1" id="muhRadio1" class="vquestioninput"/> <span>Innovating</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel ea_color"><input type="checkbox" name="answerquestion1[]" value="2" id="muhRadio2" class="vquestioninput"/> <span>Problem Solving & Decision Making</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel ea_color"><input type="checkbox" name="answerquestion1[]" value="3" id="muhRadio3" class="vquestioninput"/> <span>Engaging Change</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel ea_color"><input type="checkbox" name="answerquestion1[]" value="4" id="muhRadio4" class="vquestioninput"/> <span>Practicing Followership</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
    <?php if($current_question == 9){ ?>
   
   <ul class="answer_lists">
   <li>
   <label for="muhRadio1" class="radiobtnlabel dc_color"><input type="radio" name="answerquestion1" value="1" id="muhRadio1" class="vquestioninput"/> <span>Practicing Teamwork</span></label>
   </li>
   <li>
   <label for="muhRadio2" class="radiobtnlabel dc_color"><input type="radio" name="answerquestion1" value="2" id="muhRadio2" class="vquestioninput"/> <span>Building Partnerships</span></label>
   </li>
   <li>
   <label for="muhRadio3" class="radiobtnlabel dc_color"><input type="radio" name="answerquestion1" value="3" id="muhRadio3" class="vquestioninput"/> <span>Communicating Effectively</span></label>
   </li>
   <li>
   <label for="muhRadio4" class="radiobtnlabel dc_color"><input type="radio" name="answerquestion1" value="4" id="muhRadio4" class="vquestioninput"/> <span>Establishing & Maintaining Trust</span></label>
   </li>
   </ul>
   
   <?php } ?>
   
   <?php

		echo $this->Form->input('submission', array('id' => 'levelcompleted', 'type' => 'hidden', 'value' => '0'));
		echo $this->Form->input('current_level', array('type' => 'hidden', 'value' => $current_level));
		echo $this->Form->input('current_question', array('type' => 'hidden', 'value' => $current_question));
	?>
    <p class="answercheck"></p>
    <p id="right_answer"></p>
      <input type="submit" value="NEXT" class="load_next_vquestion" id="load_next_vquestion">   
<?php 
echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
	finalString = "<?php echo implode("", $words);?>"
</script>>