<div class="content-wrapper">
    <?php echo $this->element("vheader-htp")?>
    <div class="htp-content-holder">
        <?php echo $this->Html->image("video_htp_content.png")?>
    </div>
    <div class="letsplay-button-wrapper">
        <?php echo $this->Html->image("btn-lets-play.png", array("url" => array("controller" => "pages", "action" => "vgame_intro")))?>
    </div>
</div>
<div class="clearfix"></div>