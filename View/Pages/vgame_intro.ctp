<div class="content-wrapper">
    <?php echo $this->element("vheader-htp-intro")?>
    <div class="htp-content-holder">
        <video width="100%" height="340" controls class="videoplayer" poster="<?php echo $this->webroot?>img/1-Introduction.png">
          <source src="<?php echo $this->webroot?>videos/YYC_AUDIO1.mp4" type="video/mp4">
           Your browser does not support the video tag.
        </video>
    </div>
    <div class="next-button-wrapper">
        <?php echo $this->Html->image("btn-next.png", array("url" => array("controller" => "pages", "action" => "vgame")))?>
    </div>
</div>
<div class="clearfix"></div>