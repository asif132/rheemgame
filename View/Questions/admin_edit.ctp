<div class="questions form">
<?php echo $this->Form->create('Question', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Question'); ?></legend>
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('level_id', array('class' => 'form-control'));
		echo $this->Form->input('question_text', array('class' => 'form-control'));
		echo $this->Form->input('badge_title', array('class' => 'form-control'));
		echo $this->Form->input('badge_image', array('type' => 'file'));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-warning'));
echo $this->Form->end(); ?>
</div>