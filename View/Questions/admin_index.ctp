<div class="questions index">
	<h2><?php echo __('Questions'); ?><?php echo $this->Html->link(__('Create New Question'), array('action' => 'add'), array('class' => 'pull-right btn btn-info')); ?></h2>
	<table class="table table-striped table-hover table-condensed table-responsive">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th><?php echo $this->Paginator->sort('question_text'); ?></th>
			<th><?php echo $this->Paginator->sort('answers'); ?></th>
			<th><?php echo $this->Paginator->sort('badge_title'); ?></th>
			<th><?php echo $this->Paginator->sort('badge_image'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
    <tbody class="table-bordered">
	<?php foreach ($questions as $question): ?>
	<tr>
		<td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($question['Level']['id'], array('controller' => 'levels', 'action' => 'view', $question['Level']['id'])); ?>
		</td>
		<td><?php echo h($question['Question']['question_text']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['badge_title']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['badge_image']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id']),array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), array('class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous page'), array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default prev disabled'));
		echo $this->Paginator->numbers(array('separator' => '' ,'class' => 'btn btn-default'));
		echo $this->Paginator->next(__('next page') . ' >', array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default next disabled'));
	?>
	</div>
</div>