<div class="questions view">
<h2><?php echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['Level']['id'], array('controller' => 'levels', 'action' => 'view', $question['Level']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question Text'); ?></dt>
		<dd>
			<?php echo h($question['Question']['question_text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answers'); ?></dt>
		<dd>
			<?php echo h($question['Question']['answers']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Badge Title'); ?></dt>
		<dd>
			<?php echo h($question['Question']['badge_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Badge Image'); ?></dt>
		<dd>
			<?php echo h($question['Question']['badge_image']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Levels'), array('controller' => 'levels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Level'), array('controller' => 'levels', 'action' => 'add')); ?> </li>
	</ul>
</div>
