<div class="container">
    <div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
    <legend><?php echo __('Admin Add Setting'); ?></legend>
    <div class="adminform">
	<?php
		echo $this->Form->input('key', array('class' => 'form-control'));
		echo $this->Form->input('value', array('class' => 'form-control'));
		echo $this->Form->input('type', array('class' => 'form-control', 'options' => array('text' => 'Text', 'textarea' => 'Textarea', 'html' => 'Html')));
		echo $this->Form->input('rule', array('class' => 'form-control', 'options' => array('none' => 'None', 'integer' => 'Integer')));
	?>
    </div>
	</fieldset>
<?php echo $this->Form->submit(__('Kaydet'), array('class' => 'btn btn-warning'));?>
<?php echo $this->Form->end(); ?>
</div>
</div>