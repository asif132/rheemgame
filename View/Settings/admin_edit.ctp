<div class="container">
    <div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
	<legend><?php echo __('Admin Edit Setting'); ?></legend>
    <div class="adminform">
	<?php
		echo $this->Form->input('id', array('class' => 'form-control'));
		echo $this->Form->input('key', array('class' => 'form-control'));
		if($this->request->data['Setting']['type'] == 'html'){
			$class = 'form-control ckeditor';
			$type = 'textarea';
		} else {
			$class = 'form-control';
			$type = $this->request->data['Setting']['type'];
		}
		echo $this->Form->input('value', array('class' => $class, 'type' => $type));
	?>
    </div>
	</fieldset>
<?php echo $this->Form->submit(__('Kaydet'), array('class' => 'btn btn-warning'));?>
<?php echo $this->Form->end(); ?>
</div>
</div>