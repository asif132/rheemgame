<div class="container">
    <div class="settings index">
	<h2><?php echo __('Settings'); ?></h2>
	<table class="table table-striped table-hover table-condensed table-responsive">
    <thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('key'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
    <tbody class="table-bordered">
	<?php foreach ($settings as $setting): ?>
	<tr>
		<td><?php echo h($setting['Setting']['id']); ?>&nbsp;</td>
		<td><?php echo h($setting['Setting']['key']); ?>&nbsp;</td>
		<td><?php echo h($setting['Setting']['value']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Düzenle'), array('action' => 'edit', $setting['Setting']['id']), array('class' => 'btn btn-default')); ?>
			<?php echo $this->Form->postLink(__('Sil'), array('action' => 'delete', $setting['Setting']['id']), array('class' => 'btn btn-danger'), __('Are you sure you want to delete # %s?', $setting['Setting']['id'])); ?>
		</td>
	</tr>
	<?php endforeach; ?>
    </tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default prev disabled'));
		echo $this->Paginator->numbers(array('separator' => '' ,'class' => 'btn btn-default'));
		echo $this->Paginator->next(__('next') . ' >', array('class' => 'btn btn-default'), null, array('class' => 'btn btn-default next disabled'));
	?>
	</div>
</div>
</div>