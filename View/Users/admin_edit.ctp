<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Change password'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username', array('class' => 'form-control'));
		echo $this->Form->input('email', array('class' => 'form-control'));
	?>
	</fieldset>
<?php 
echo $this->Form->submit(__('Save'), array('class' => 'btn btn-warning'));
echo $this->Form->end(); ?>
</div>