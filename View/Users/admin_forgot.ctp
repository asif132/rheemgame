    <h2><?php echo __('Forgot Password');?></h2>
    <?php echo $this->Form->create('User'); ?>
    <div class="input-group tenpx">
        <span class="input-group-addon">@</span>
        <?php echo $this->Form->input('email',array('div' => false, 'label' => false, 'class' => 'form-control', 'placeHolder' => 'Email', 'error' => false)); ?>
    </div>
    <div class="clearfix tenpx"></div>
    <?php echo $this->Form->submit(__('Create new password'), array('class' => 'btn btn-warning'));?>
    <div class="clearfix tenpx"></div>
    <?php echo $this->Form->end(); ?>