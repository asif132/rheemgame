    <h2>Login</h2>
    <?php echo $this->Form->create('User'); ?>
    <div class="input-group tenpx">
        <span class="input-group-addon glyphicon glyphicon-user"></span>
        <?php echo $this->Form->input('username',array('div' => false, 'label' => false, 'class' => 'form-control', 'placeHolder' => 'Username or Email')); ?>
    </div>
    <div class="input-group tenpx">
        <span class="input-group-addon glyphicon glyphicon-lock"></span>
        <?php echo $this->Form->password('password',array('div' => false, 'label' => false, 'class' => 'form-control', 'placeHolder' => 'Password')); ?>
    </div>
    <div class="tenpx">

    <?php echo $this->Html->link(__('Forgot Password'), array('admin' => true, 'controller' => 'users', 'action' => 'forgot'), array('class' => 'pull-right fivepx'));?>
    <?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-warning'));?>
    </div>
    <div class="clearfix tenpx"></div>
    <?php echo $this->Form->end(); ?>