    <h2><?php echo __('Set New Password');?></h2>
    <?php echo $this->Form->create('User'); ?>
    <div class="input-group tenpx">
        <span class="input-group-addon glyphicon glyphicon-lock"></span>
        <?php echo $this->Form->password('password',array('div' => false, 'label' => false, 'class' => 'form-control', 'placeHolder' => 'Password')); ?>
    </div>
    <div class="input-group tenpx">
        <span class="input-group-addon glyphicon glyphicon-lock"></span>
        <?php echo $this->Form->password('password2',array('div' => false, 'label' => false, 'type' => 'password', 'class' => 'form-control', 'placeHolder' => 'Re-type Password')); ?>
    </div>
    <div class="clearfix tenpx"></div>
    <?php echo $this->Form->submit(__('Change'), array('class' => 'btn btn-warning'));?>
    <div class="clearfix tenpx"></div>
    <?php echo $this->Form->end(); ?>