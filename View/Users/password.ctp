<div class="container">
    <div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Change Password'); ?></legend>
	<?php
		echo $this->Form->input('password', array('label' => 'New password', 'class' => 'form-control'));
		echo $this->Form->input('password2', array('label' => 'Retype new password', 'div' => false, 'class' => 'form-control', 'type' => 'password'));
	?>
	</fieldset>
<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-danger'));?>
<?php echo $this->Form->end(); ?>
</div>
</div>