<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	 public $components = array(
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session',
		'Cookie',
		'Flash'
    );

	
	public $uses = array('Setting');
	
	public function beforeFilter() {
		
		$this->Setting->get_settings(); 
		$this->set('site_settings', $this->Setting->site_settings);

		$this->Cookie->key = '@(w4SAv8(Lq+x!adra=8JsGbgR3!2qs*&s#HKi%)s~#e@s323!@*SI#$^';
		$this->Cookie->httpOnly = true;

		$this->Auth->allow();
		if ((isset($this->params['prefix']) && $this->params['prefix'] == 'admin')) {

			if($this->params['prefix'] == 'admin')
				$this->layout = "admin";

			if (!$this->Auth->loggedIn() && $this->Cookie->read('user_info')) {
			
				$cookie = $this->Cookie->read('user_info');
				$this->loadModel('User');
				$user = $this->User->find('first', array(
					'conditions' => array(
						'User.id' => $cookie['id'],
						'User.password' => $cookie['password']
					)
				));
				if ($user && !$this->Auth->login($user['User'])) {
					$this->redirect($this->Auth->logout());
				}
			}
			
			$this->member = $this->Session->read('Auth.User');
			$this->set('member',$this->member);

			if($this->params['controller'] != 'user' && !($this->params['action'] == 'admin_login' || $this->params['action'] == 'admin_forgot' || $this->params['action'] == 'admin_recover' )&& !($this->member['id'] > 0 ) ){
				return $this->redirect(array('controller' => 'users', 'prefix' => 'admin', 'action' => 'login'));
			}
			
			$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => true);
			$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
			$this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'index', 'admin' => true);
			
		}
	}

	public function slugify($str, $delimiter='-') {

		$char_search = array('İ', 'ı');
		$char_replace = array('I', 'i');

		$str = str_ireplace($char_search, $char_replace, $str);

		setlocale(LC_ALL, 'en_US.UTF-8');
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		return $clean;
		
	}

	public function slugifyFile($filename = "") {

		$extension = end(explode('.', $filename));
		return $this->slugify(substr($filename,0, -1*(1+strlen($extension)))).'.'.$extension;
		
	}
}
