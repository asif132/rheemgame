<?php
App::uses('AppController', 'Controller');
/**
 * Pages Controller
 *
 * @property Page $Page
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'RequestHandler');



	public function view( $slug = null ) {
		$options = array('conditions' => array('slug' => $slug));

		$page = $this->Page->find('first', $options);

		$this->set('page', $page);
		
	}

	public function home( ) {

		$options = array('conditions' => array('slug' => 'home'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "main-screen");
		
	}

	public function how_to_play( ) {

		$options = array('conditions' => array('slug' => 'home'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "how-to-play");
		
	}

	public function how_to_play2( ) {

		$options = array('conditions' => array('slug' => 'home'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "how-to-play");
		
	}

	public function game(){

		$this->set("html_class", "game-screen");
		$this->loadModel("Question");
		$this->loadModel("Level");

		$levels = $this->Level->find("all", 
			array(
				'order' => array('Level.level_number' => 'asc'),
				'contain' => array(
					'Question' => array('order' => array("Question.question_number" => 'asc') )
					) 
				) 
			);
		$this->set("levels", $levels);

		$cookie_levels_data = $this->Cookie->read('levels_data');
		$cookie_game_data = $this->Cookie->read('game_data');

		// Check if user has completed a level
		if($this->request->data["Question"]['submission'] == "1"){

			$cookie_levels_data[$this->request->data["Question"]['current_level']][$this->request->data["Question"]['current_question']] = $this->request->data["Question"]['submission'];
			$this->Cookie->write('levels_data', $cookie_levels_data, false, '24 hours');

			$current_level = $this->request->data["Question"]['current_level'];
			$current_question = $this->request->data["Question"]['current_question'];
			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question);
			$this->set("html_class", "badge-earned-screen");
			$this->render("badge_earned");
			return;

		}

		// Check if user timed out
		if($this->request->data["Question"]['submission'] == "2"){
			
			$cookie_levels_data[$this->request->data["Question"]['current_level']][$this->request->data["Question"]['current_question']] = $this->request->data["Question"]['submission'];
			$this->Cookie->write('levels_data', $cookie_levels_data, false, '24 hours');
			
		}

		// Check if fuel up scene seen
		if($this->request->data["Question"]['fuelup_seen'] == 1 ){
			$cookie_game_data['fuelup_seen_'.$this->request->data["Question"]['current_level']] = 1;
			$this->Cookie->write('game_data', $cookie_game_data, false, '24 hours');
		}

		// Check if badges completed scene seen
		if($this->request->data["Question"]['badges_collected_seen'] == 1 ){
			$cookie_game_data['badges_collected_seen_'.$this->request->data["Question"]['current_level']] = 1;
			$this->Cookie->write('game_data', $cookie_game_data, false, '24 hours');
		}

		$last_level = count($cookie_levels_data);
		$last_question = count($cookie_levels_data[$last_level]);
//var_dump($cookie_game_data);die;
		$current_level = 1;
		$current_question = 1;

		// Lets Check if user has just started playing or if user has already started a game.
		if( $cookie_game_data == null ){

			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question);
			$this->set("html_class", "fuel-up-screen");
			$this->render("fuel_up");

		}elseif($cookie_levels_data == null ){
			$question = $levels[$current_level-1]['Question'][$current_question-1];

			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question);
			$this->set("question", $question);
		}else{

			if( $last_question + 1 <= count($levels[$last_level-1]['Question']) ){
				$current_question = $last_question + 1;
				$current_level = $last_level;
			}else{
				//Rana work started from here
				// Lets Check if user has completed all of the questions
				foreach ($cookie_levels_data[$last_level] as $key => $value) {
					if( $value == 2 ){
						$failed_question = $key;
						break;
					}
				}

				if( $failed_question != null ){
					$current_level = $last_level;
					$current_question = $failed_question;
				}else{

					// Show bages Collected screen.
					if($cookie_game_data['badges_collected_seen_'.$last_level] == null ){

						$this->set("current_level", $last_level);
						$this->set("current_question", $current_question);
						$this->set("html_class", "badges-completed-screen");
						$this->set("cookie_levels_data", $cookie_levels_data);
						$this->render("badges_collected");
					}else
					// Show fuel up screen for new level
					if($cookie_game_data['fuelup_seen_'.($last_level+1)] == null && $last_level + 1 <= count($levels) ){
						$current_level = $last_level + 1;

						$this->set("current_level", $current_level);
						$this->set("current_question", $current_question);
						$this->set("html_class", "fuel-up-screen");
						$this->render("fuel_up");
					}
					else
					// Display new level's first question.
					if($cookie_game_data['badges_collected_seen_'.$last_level] == 1 && $cookie_game_data['fuelup_seen_'.($last_level+1)] == 1 && $last_level + 1 <= count($levels) ){
						$current_level = $last_level + 1;
						$current_question = 1;
					}else{
						$cookie_game_data['badges_collected_seen_'.$last_level] = null;
						$this->Cookie->write('game_data', $cookie_game_data, false, '24 hours');
						return $this->redirect(array("controller" => "pages", "action" => "home"));
					}
				}

			}


			$level_status = $cookie['levels_data'][$last_level][$last_question];
			$question = $levels[$current_level-1]['Question'][$current_question-1];

			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question);
			$this->set("question", $question);
		}

	}

	public function admin_index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
		$this->set('page', $this->Page->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add( $tip = null ) {

		if($tip == null){
			return $this->redirect(array('action' => 'index'));
		}
		
		if ($this->request->is('post')) {
			$this->Page->create();

			$data = $this->request->data;
			$data['Page']['tip'] = $tip;

			if( $data['Page']['dosya']['size'] > 0 && $data['Page']['dosya']['name'] != ''){
				$data['Page']['dosya']['name'] = $this->slugifyFile($data['Page']['dosya']['name']);
				$data['Page']['resim'] = $data['Page']['dosya'];
			}

			$data['Page']['icerik'] = html_entity_decode($data['Page']['icerik'], ENT_COMPAT, "UTF-8");
			$data['Page']['slug'] = $this->slugify($data['Page']['baslik']);

			if ($this->Page->save($data)) {

				$pid = $this->Page->getInsertID();

				$options = array('conditions' => array('slug' => $data['Page']['slug']));
				$page_count = $this->Page->find('count', $options);

				if($page_count > 1){
					$this->Page->id = $pid;
					$this->Page->save( array('slug' => $data['Page']['slug']."-".$pid) );
				}
				$this->Flash->success(__('The page has been saved.'));

				if ($data['Page']['tip'] == 'haber')
					return $this->redirect(array('action' => 'haberler'));
				else
					return $this->redirect(array('action' => 'sayfalar'));
			} else {
				$this->Flash->error(__('The page could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$data = $this->request->data;

			if( $data['Page']['dosya']['size'] > 0 && $data['Page']['dosya']['name'] != ''){
				$data['Page']['dosya']['name'] = $this->slugifyFile($data['Page']['dosya']['name']);
				$data['Page']['resim'] = $data['Page']['dosya'];
			}
			
			$data['Page']['icerik'] = html_entity_decode($data['Page']['icerik'], ENT_COMPAT, "UTF-8");

			if ($this->Page->save($data)) {
				$this->Flash->success(__('The page has been saved.'));
				if ($this->request->data['Page']['tip'] == 'haber')
					return $this->redirect(array('action' => 'haberler'));
				else
					return $this->redirect(array('action' => 'sayfalar'));
			} else {
				$this->Flash->error(__('The page could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
			$this->request->data = $this->Page->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Page->delete()) {
			$this->Flash->success(__('The page has been deleted.'));
		} else {
			$this->Flash->error(__('The page could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
	///////////////////////////////////
	
	public function vgamehome( ) {

		$options = array('conditions' => array('slug' => 'vgamehome'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "main-screen");
		
	}

	
	public function vgame(){
		
       $this->layout = 'vgame';
	   $this->set("html_class", "game-screen");
		
		//Rana
		
		
		
		if($this->request->data['doAction'] == "checkanswer"){
		sleep(1);
		$json = array();
		$current_question = $this->request->data['current_question'];
		$givenAnswer = $this->request->data['answer'];
			 
			 
			$json['success'] = 0;
			 
			if($current_question == 1)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 4)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Max came in with great ideas on how to expand YYC\'s brand. He did the research and was ready to present his ideas to the team. Max demonstrated initiative and determination to achieve objectives, and he understood the need to take a risk with rebranding.';
					
				}
				
				
			}
			
			if($current_question == 2)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 3)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
$json['answer_text'] = 'Max came in with great ideas on how to expand YYC\'s brand. He did the research and was ready to present his ideas to the team. Max was focused on adopting new approaches in response to changes in the business environment.';
					
				}
				
				
			}
			
			
			
			
			
			
			
			
			if($current_question == 3)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				
				$givenAnswerArr = explode(',',$givenAnswer);
				
				unset($givenAnswerArr[0]);
				
				if($givenAnswerArr[1] == 1 and $givenAnswerArr[2] == 4)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Max is new to the team and was not listening to his team members, Max was not clued in to the team\'s emotions, he could be perceived as "steamrolling" the conversation.';
					
				}
				
				
			}
			
			if($current_question == 4)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 1)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Phillip used non-traditional ideas to meet the requirement for producing a new chocolate flavor that could be produced on existing manufacturing lines, and he used available resources creatively.';
					
				}
				
				
			}
			
			if($current_question == 5)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 1)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'When confronted about not including all of the stakeholders on the project team, Philip became defensive and blamed others.';
					
				}
				
				
			}
			
			if($current_question == 6)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 2)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Brie immediately called her internal team to begin relationship repair with YYC\'s customers.';
					
				}
				
				
			}
			
			if($current_question == 7)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 3)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Ideally Brie would try to listen and understand Kurt\'s concerns rather than stating he would have to do what she said or she would escalate the issue. Brie should focus on actions that build relationships with Kurt rather than words and actions that diminish the relationship.';
					
				}
				
				
			}
			
			if($current_question == 8)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				
				$givenAnswerArr = explode(',',$givenAnswer);
				
				unset($givenAnswerArr[0]);
				
				if($givenAnswerArr[1] == 1 and $givenAnswerArr[2] == 2)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Fred presented a suggestion to his boss on how to streamline their chocolate lines and achieve operational efficiencies.';
					
				}
				
				
			}
			
			if($current_question == 9)
			{
				
				$json['response_text'] = 'That\'s not correct. Try again';
				$json['answer_text'] = '';
				
				if($givenAnswer == 3)
				{
					$json['success'] = 1;
					$json['response_text'] = 'That is correct!';
					$json['answer_text'] = 'Fred arrived at the meeting late and was not prepared. If he had questions about who was presenting the information, he should have asked prior to the meeting.';
					
				}
				
				
			}
			
			echo json_encode($json);
			die;
			return;
		}
		
		
		$current_level = 1;
		$current_question = 1;
		
		
		$this->set("current_level", $current_level);
		$this->set("current_question", $current_question);
		
		$cookie_game_data = $this->Cookie->read('current_question_number');
		
		if($this->request->data["Question"]['submission'] == "1"){

			$cookie_levels_data[$this->request->data["Question"]['current_level']][$this->request->data["Question"]['current_question']] = $this->request->data["Question"]['submission'];
			

			$current_question = $this->request->data["Question"]['current_question'];
			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question+1);
			$this->Cookie->write('current_question_number', $current_question+1, false, '24 hours');
			return;

		}
		
		if( $cookie_game_data == null ){
			$this->set("current_level", $current_level);
			$this->set("current_question", $current_question);
			$this->Cookie->write('game_data', $cookie_game_data, false, '24 hours');

		}else{
			$current_question=$cookie_game_data;
			//$current_question=$cookie_game_data['current_question_number'];
			$this->set("current_question", $current_question);
			$this->set("current_level", $current_level);
			
		}


	}
	
	
	public function vgame_how_to_play( ) {

		$options = array('conditions' => array('slug' => 'home'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "how-to-play");
		
	}
	
	public function vgame_intro( ) {

		$options = array('conditions' => array('slug' => 'home'));

		$page = $this->Page->find('first', $options);
		
		$this->set('page', $page);
		$this->set('html_class', "vgame-intro");
		
	}
	
	
}
