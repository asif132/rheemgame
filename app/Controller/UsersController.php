<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function admin_login() {
		$this->layout = 'admin_login';
		
		if ($this->request->is('post')) {
			$this->User->recursive = -1;
			$options = array('conditions' => array('OR' => array('User.email' => $this->request->data['User']['username'], 'User.username' => $this->request->data['User']['username']), 'AND' => array('User.password' => AuthComponent::password($this->request->data['User']['password'])) ));
			$user = $this->User->find('first',$options);
			
			if ($this->Auth->login($user['User'])){
				
				$cookie_data = array('id' => $user['User']['id'], 'username' => $user['User']['username'], 'password' => $user['User']['password']);
				$this->Cookie->write('user_info', $cookie_data, false, '24 hours');
				
				return $this->redirect($this->Auth->redirect());
			}
			$this->Flash->error(__('Your username or password was incorrect.'));
		}
	}
	
	public function admin_logout() {
		
		$this->Session->destroy();
		$this->Cookie->delete('user_info');
		//$this->Flash->success('');
		$this->redirect($this->Auth->logout());
	}

	public function admin_password($id = null) {
		
		$this->autoRender = false;
		
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			if ($this->request->data['User']['password'] == $this->request->data['User']['password2']){
				$data = array('id' => $id, 'password' => $this->request->data['User']['password']);
				$this->User->save($data);
				$this->Flash->success(__('Password has been updated.'));
				return $this->redirect(array('action' => 'index'));
			}else{
				$this->Flash->error(__('New passwords does not match.'));
			}
			
		}
		
		$this->render('password');
	}

	public function admin_forgot(){
		
		$this->layout = 'admin_login';
		
		if ($this->request->is('post')) {
			
			$options = array('conditions' => array('User.email' => $this->request->data['User']['email']));
			$user = $this->User->find('first', $options);
			
			if($user != null){
				
				$key = Security::hash(uniqid().'8M!d?c%g'.time());
				$resetlink = Router::url(array('controller' => 'users', 'action' => 'recover', $key), true);
				
				$this->User->id = $user['User']['id'];
				$this->User->save(array('forgot_code' => $key, 'forgot_time' => date('Y-m-d H:i:s')));
				
				$mailcontent = __('Hello %s,', array($user['User']['username'])).'<br/><br/>'.__('Please use the following link to reset your password:').'<br/>'.$resetlink.' <br/><br/>'.__('If you didn\'t request a password reset, you can ignore this message and your password will not be changed.').'<br/><br/>'.$this->Setting->confarr['site_name'];
				App::uses('CakeEmail', 'Network/Email');
				$Email = new CakeEmail();
				$Email->from(array($this->Setting->confarr['site_mail'] => $this->Setting->confarr['site_name']));
				$Email->to($user['User']['email']);
				$Email->subject(__('Password reset on %s', array($this->Setting->confarr['site_name'])));
				$Email->emailFormat('html');
				$Email->send($mailcontent);
				
				$this->Session->setFlash(__('Forgot password reminder has been sent please check your mailbox.'), 'default', array(), 'warning');
				return $this->redirect(array('controller' => 'users', 'action' => 'login'));
				
			}else{
				$this->Session->setFlash(__('This email address is not registered.'), 'default', array(), 'error');
			}
		}
		
	}
	
	public function admin_recover( $token = null ){
		
		$this->layout = 'admin_login';
		
		$user = $this->User->find('first', array('conditions' => array('forgot_code' => $token, 'forgot_time BETWEEN NOW() -INTERVAL 1 DAY AND NOW()')));
		if($user != NULL){
			if ($this->request->is('post')){
				$npass = $this->request->data['User']['password'];
				$npass2 = $this->request->data['User']['password2'];
				if($npass == $npass2){
					$this->User->id = $user['User']['id'];
					$this->User->save(array('password' => $npass, 'forgot_code' => '', 'forgot_time' => NULL));
					
					$this->Session->setFlash(__('Your password has been updated.'), 'default', array(), 'success');
					if($this->loginUser($user))
						return $this->redirect($this->Auth->redirect());
					else
						return $this->redirect(array('controller' => 'users', 'action' => 'login'));
					
				}else{
					$this->Session->setFlash(__('Your new password doesn\'t seem to be matching.'), 'default', array(), 'error');
				}
			}
		}else{
			$this->Session->setFlash(__('This is not a valid link.'), 'default', array(), 'error');
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
		
	}

}
